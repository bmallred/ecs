# Entity Component System

Simple and naive entity component system (ECS) written for Go.

> Entity–component–system (ECS) is an architectural pattern that is mostly used in game development. ECS follows the composition over inheritance principle that allows greater flexibility in defining entities where every object in a game's scene is an entity (e.g. enemies, bullets, vehicles, etc.). Every entity consists of one or more components which add behavior or functionality. Therefore, the behavior of an entity can be changed at runtime by adding or removing components. This eliminates the ambiguity problems of deep and wide inheritance hierarchies that are difficult to understand, maintain and extend. Common ECS approaches are highly compatible and often combined with data-oriented design techniques. 
[Wikipedia](https://en.wikipedia.org/wiki/Entity_component_system)

## The nature of this document

This program was converted to **literate programming** with the help of [lmt](https://github.com/driusan/lmt) by **Dave MacFarlane**.

To build from this document you may do the following:

``` shell
lmt README.md;
gofmt -w *.go;
```

## Entities

An entity is a general purpose object and it consists of a unique identifier. This naturally leads us towards creating an interface which will expose the unique identifier.

``` go "entity interface"
// Identifier interface to retrieve the entity's unique identifier.
type Identifier interface {
	ID() uint64
}
```

From there we will create a simple entity structure to store our integer (unique identifier)

``` go "entity structure"
// Entity is a light structure composed of components.
type Entity struct {
	id         uint64
}
```

It is common in game systems to have some form of an entity manager which handles adding and removing entities. For this we will simply add an interface for it but not define it any further.

``` go "entity interface" +=
// EntityManager interface is used to manage storage of entities within
// the world and systems.
type EntityManager interface {
	Add(entity *Entity)
	Remove(entity *Entity)
}
```

It will be common to create a new entity, but one which has a truly unique identifier. We will have to make sure the incrementing of the identifier is in sync to prevent collisions.

``` go "entity methods"
// NewEntity creates a basic entity with a unique identifier.
func NewEntity() *Entity {
	return &Entity{id: atomic.AddUint64(&idInc, 1)}
}

// ID returns the entity unique identifier.
func (entity *Entity) ID() uint64 {
	return entity.id
}
```

The use of `atomic` here requires we import a few namespaces

``` go "entity import"
import (
	"sync"
	"sync/atomic"
)
```

as well as declare a few variables

``` go "entity var"
var (
	counterLock sync.Mutex
	idInc       uint64
)
```

## Components

A component represents raw data for one aspect of the object.

``` go "component interface"
// Component interface describes a component which may be
// associated with an entity. These are purely just data used
// to describe the state.
type Component interface {
	Type() string
}
```

Now we need to adjust our entity structure to include an array of components

``` go "entity structure"
// Entity is a light structure composed of components.
type Entity struct {
	id         uint64
	components []Component
}
```

And the entity should also provide access to it's components for informational purposes and for manipulation.

``` go "entity methods" +=
// Components returns all associated components.
func (entity *Entity) Components() []Component {
	return entity.components
}

<<<add component to entity>>>
<<<remove component from entity>>>
<<<get component of entity>>>
<<<filter entities by similar components>>>
```

First, we need to be able to retrieve a component from the entity.

``` go "get component of entity"
// GetComponent attempts to retrieve the requested component by the type. If none
// were found `nil` is returned.
func (entity *Entity) GetComponent(component Component) Component {
	for _, assigned := range entity.components {
		if assigned.Type() == component.Type() {
			return assigned
		}
	}

	return nil
}
```


Next, we will simply handle adding new components (variable amount) to the entity. However, we must take care the component is not already assigned to the entity.

``` go "add component to entity"
// AddComponent adds one or more components to the entity.
func (entity *Entity) AddComponent(components ...Component) {
	for _, component := range components {
		if entity.GetComponent(component) == nil {
			entity.components = append(entity.components, component)
		}
	}
}
```

And now do the same but only in regards of removing the components.

``` go "remove component from entity"
// RemoveComponent removes one or more components from the entity.
func (entity *Entity) RemoveComponent(components ...Component) {
	marked := []int{}
	for i, assigned := range entity.components {
		for _, component := range components {
			if assigned.Type() == component.Type() {
				marked = append(marked, i)
				break
			}
		}
	}

	for i := len(marked) - 1; i >= 0; i-- {
		entity.components = append(entity.components[:mark], entity.components[mark+1:]...)
	}
}
```

It is common within systems to find entities which may have a unique combination of components. Looping through an array of entities and checking the requested components is pretty straight forward, but it is important to point out the order of components may help reduce computational costs. Ordering by the rarest (or least used) component to most common component will require less cycles.

``` go "filter entities by similar components"
// FilterComponent returns an array of entities which have all of the requested components.
func FilterComponent(entities []*Entity, components ...Component) []*Entity {
	filtered := []*Entity{}

	for _, entity := range entities {
		if every(entity, components...) {
			filtered = append(filtered, entity)
		}

	}

	return filtered
}

// FilterBy returns an array of entities which have met the predicate.
func FilterBy(entities []*Entity, predicate func(entity *Entity) bool) []*Entity {
	result := []*Entity{}
	for _, entity := range entities {
		if predicate(entity) {
			result = append(result, entity)
		}
	}
	return result
}

// Checks to see if an entity has every component requested or not.
//
// This is set to return early so when looking for multiple component
// types it is best provide them in the order of rarest to most common.
func every(entity *Entity, components ...Component) bool {
	for _, component := range components {
		if entity.GetComponent(component) == nil {
			return false
		}
	}

	return true
}
```

## Systems

A system recognizes entities based on specific components and processes the data.

``` go "system interface"
import "time"

// System interface provides the groupind of logic to be performed on entities
// based on their associated components.
type System interface {
	Update(delta time.Duration, entities []*Entity)
}
```

## File structure

In this section we define the basic file structure used. This could all be done in one file obviously but for readability it has been separated in to three separate files corresponding with each type.

The base package is called `ecs` so it will need to go in to each file.

``` go "package name"
package ecs
```

### Entity

``` go entity.go
<<<package name>>>
<<<entity import>>>
<<<entity var>>>
<<<entity interface>>>
<<<entity structure>>>
<<<entity methods>>>
```

### Component

``` go component.go
<<<package name>>>
<<<component interface>>>
```

### System

``` go system.go
<<<package name>>>
<<<system interface>>>
```
