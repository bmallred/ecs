package ecs

// Component interface describes a component which may be
// associated with an entity. These are purely just data used
// to describe the state.
type Component interface {
	Type() string
}
