package ecs

import "time"

// System interface provides the groupind of logic to be performed on entities
// based on their associated components.
type System interface {
	Update(delta time.Duration, entities []*Entity)
}
