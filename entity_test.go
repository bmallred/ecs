package ecs

import (
	"testing"
)

type ComponentA struct{}

func (component ComponentA) Type() string {
	return "ComponentA"
}

type ComponentB struct{}

func (component ComponentB) Type() string {
	return "ComponentB"
}

type ComponentC struct{}

func (component ComponentC) Type() string {
	return "ComponentC"
}

type ComponentD struct{}

func (component ComponentD) Type() string {
	return "ComponentD"
}

func TestNewEntity(t *testing.T) {
	entities := []*Entity{}
	for i := 0; i < 100; i++ {
		entities = append(entities, NewEntity())
	}

	if len(entities) != 100 {
		t.Fatalf("expected: %d actual: %d", 100, len(entities))
	}

	for _, entity := range entities {
		matched := FilterBy(entities, func(a *Entity) bool {
			return a.ID() == entity.ID()
		})

		if len(matched) != 1 {
			t.Fatalf("expected: %d actual: %d", 1, len(matched))
		}
	}
}

func TestAddComponent(t *testing.T) {
	cases := []struct {
		name     string
		add      []Component
		expected int
	}{
		{"Add one", []Component{ComponentA{}}, 1},
		{"Add two", []Component{ComponentA{}, ComponentB{}}, 2},
		{"Add four", []Component{ComponentA{}, ComponentB{}, ComponentC{}, ComponentD{}}, 4},
	}

	for _, c := range cases {
		entity := NewEntity()
		if len(c.add) > 0 {
			entity.AddComponent(c.add...)
		}
		actual := len(entity.Components())
		if actual != c.expected {
			t.Fatalf("%s: expected: %d actual: %d", c.name, c.expected, actual)
		}
	}
}

func TestRemoveComponent(t *testing.T) {
	cases := []struct {
		name     string
		add      []Component
		remove   []Component
		expected int
	}{
		{"Empty", []Component{}, []Component{ComponentA{}}, 0},
		{"None", []Component{ComponentA{}}, []Component{ComponentA{}}, 0},
		{"Leave first", []Component{ComponentA{}, ComponentB{}}, []Component{ComponentB{}}, 1},
		{"Leave last", []Component{ComponentA{}, ComponentB{}}, []Component{ComponentA{}}, 1},
		{"Remove non-existent", []Component{ComponentA{}, ComponentB{}}, []Component{ComponentC{}}, 2},
		{"Remove multiple 1", []Component{ComponentA{}, ComponentB{}, ComponentC{}, ComponentD{}}, []Component{ComponentA{}, ComponentC{}}, 2},
		{"Remove multiple 2", []Component{ComponentA{}, ComponentB{}, ComponentC{}, ComponentD{}}, []Component{ComponentB{}, ComponentD{}}, 2},
	}

	for _, c := range cases {
		entity := NewEntity()
		if len(c.add) > 0 {
			entity.AddComponent(c.add...)
		}
		if len(c.remove) > 0 {
			entity.RemoveComponent(c.remove...)
		}
		actual := len(entity.Components())
		if actual != c.expected {
			t.Fatalf("%s: expected: %d actual: %d", c.name, c.expected, actual)
		}
	}
}

func TestGetComponent(t *testing.T) {
	cases := []struct {
		name   string
		add    []Component
		find   Component
		isNull bool
	}{
		{"Empty", []Component{}, ComponentA{}, true},
		{"None", []Component{ComponentB{}}, ComponentA{}, true},
		{"One", []Component{ComponentA{}, ComponentB{}}, ComponentB{}, false},
		{"Has two but returns one", []Component{ComponentA{}, ComponentA{}}, ComponentA{}, false},
	}

	for _, c := range cases {
		entity := NewEntity()
		if len(c.add) > 0 {
			entity.AddComponent(c.add...)
		}
		actual := entity.GetComponent(c.find)
		if (actual != nil && c.isNull) || (actual == nil && !c.isNull) {
			expected := "<nil>"
			if !c.isNull {
				expected = "not <nil>"
			}
			t.Fatalf("%s: expected: %s got: %v", c.name, expected, actual)
		}
	}
}

func TestFilterComponent(t *testing.T) {
	cases := []struct {
		name     string
		add      []Component
		find     []Component
		expected int
	}{
		{"Empty", []Component{}, []Component{ComponentA{}}, 0},
		{"None", []Component{ComponentB{}}, []Component{ComponentA{}}, 0},
		{"First", []Component{ComponentA{}, ComponentB{}, ComponentC{}}, []Component{ComponentA{}}, 1},
		{"Middle", []Component{ComponentA{}, ComponentB{}, ComponentC{}}, []Component{ComponentB{}}, 1},
		{"Last", []Component{ComponentA{}, ComponentB{}, ComponentC{}}, []Component{ComponentC{}}, 1},
		{"Match all", []Component{ComponentA{}, ComponentB{}}, []Component{ComponentA{}, ComponentB{}}, 1},
		{"Missing one", []Component{ComponentA{}, ComponentB{}}, []Component{ComponentA{}, ComponentB{}, ComponentC{}}, 0},
	}

	for _, c := range cases {
		entity := &Entity{}
		if len(c.add) > 0 {
			entity.AddComponent(c.add...)
		}
		actual := len(FilterComponent([]*Entity{entity}, c.find...))
		if actual != c.expected {
			t.Fatalf("%s: expected: %d actual: %d", c.name, c.expected, actual)
		}
	}
}
